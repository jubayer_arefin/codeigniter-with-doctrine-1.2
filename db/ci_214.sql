/*
Navicat MySQL Data Transfer

Source Server         : WAMP
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : ci_214

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2013-09-23 18:04:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for addresses
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `address` text NOT NULL,
  `road` varchar(10) NOT NULL,
  `area` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `country_id` int(11) unsigned NOT NULL,
  `current` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `Address_User` (`user_id`),
  KEY `Address_Country` (`country_id`),
  CONSTRAINT `Address_Country` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `Address_User` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addresses
-- ----------------------------

-- ----------------------------
-- Table structure for countries
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'Andorra');
INSERT INTO `countries` VALUES ('2', 'United Arab Emirates');
INSERT INTO `countries` VALUES ('3', 'Afghanistan');
INSERT INTO `countries` VALUES ('4', 'Antigua and Barbuda');
INSERT INTO `countries` VALUES ('5', 'Anguilla');
INSERT INTO `countries` VALUES ('6', 'Albania');
INSERT INTO `countries` VALUES ('7', 'Armenia');
INSERT INTO `countries` VALUES ('8', 'Netherlands Antilles');
INSERT INTO `countries` VALUES ('9', 'Angola');
INSERT INTO `countries` VALUES ('10', 'Argentina');
INSERT INTO `countries` VALUES ('11', 'American Samoa');
INSERT INTO `countries` VALUES ('12', 'Austria');
INSERT INTO `countries` VALUES ('13', 'Australia');
INSERT INTO `countries` VALUES ('14', 'Aruba');
INSERT INTO `countries` VALUES ('15', 'Azerbaijan');
INSERT INTO `countries` VALUES ('16', 'Bosnia and Herzegowi');
INSERT INTO `countries` VALUES ('17', 'Barbados');
INSERT INTO `countries` VALUES ('18', 'Bangladesh');
INSERT INTO `countries` VALUES ('19', 'Belgium');
INSERT INTO `countries` VALUES ('20', 'Burkina Faso');
INSERT INTO `countries` VALUES ('21', 'Bulgaria');
INSERT INTO `countries` VALUES ('22', 'Bahrain');
INSERT INTO `countries` VALUES ('23', 'Burundi');
INSERT INTO `countries` VALUES ('24', 'Benin');
INSERT INTO `countries` VALUES ('25', 'Bermuda');
INSERT INTO `countries` VALUES ('26', 'Brunei Darussalam');
INSERT INTO `countries` VALUES ('27', 'Bolivia');
INSERT INTO `countries` VALUES ('28', 'Brazil');
INSERT INTO `countries` VALUES ('29', 'Bahamas');
INSERT INTO `countries` VALUES ('30', 'Bhutan');
INSERT INTO `countries` VALUES ('31', 'Bouvet Island');
INSERT INTO `countries` VALUES ('32', 'Botswana');
INSERT INTO `countries` VALUES ('33', 'Belarus');
INSERT INTO `countries` VALUES ('34', 'Belize');
INSERT INTO `countries` VALUES ('35', 'Canada');
INSERT INTO `countries` VALUES ('36', 'Cocos (Keeling) Isla');
INSERT INTO `countries` VALUES ('37', 'Central African Repu');
INSERT INTO `countries` VALUES ('38', 'Congo');
INSERT INTO `countries` VALUES ('39', 'Switzerland');
INSERT INTO `countries` VALUES ('40', 'Cote D\'Ivoire');
INSERT INTO `countries` VALUES ('41', 'Cook Islands');
INSERT INTO `countries` VALUES ('42', 'Chile');
INSERT INTO `countries` VALUES ('43', 'Cameroon');
INSERT INTO `countries` VALUES ('44', 'China');
INSERT INTO `countries` VALUES ('45', 'Colombia');
INSERT INTO `countries` VALUES ('46', 'Costa Rica');
INSERT INTO `countries` VALUES ('47', 'Cuba');
INSERT INTO `countries` VALUES ('48', 'Cape Verde');
INSERT INTO `countries` VALUES ('49', 'Christmas Island');
INSERT INTO `countries` VALUES ('50', 'Cyprus');
INSERT INTO `countries` VALUES ('51', 'Czech Republic');
INSERT INTO `countries` VALUES ('52', 'Germany');
INSERT INTO `countries` VALUES ('53', 'Djibouti');
INSERT INTO `countries` VALUES ('54', 'Denmark');
INSERT INTO `countries` VALUES ('55', 'Dominica');
INSERT INTO `countries` VALUES ('56', 'Dominican Republic');
INSERT INTO `countries` VALUES ('57', 'Algeria');
INSERT INTO `countries` VALUES ('58', 'Ecuador');
INSERT INTO `countries` VALUES ('59', 'Estonia');
INSERT INTO `countries` VALUES ('60', 'Egypt');
INSERT INTO `countries` VALUES ('61', 'Western Sahara');
INSERT INTO `countries` VALUES ('62', 'Eritrea');
INSERT INTO `countries` VALUES ('63', 'Spain');
INSERT INTO `countries` VALUES ('64', 'Ethiopia');
INSERT INTO `countries` VALUES ('65', 'Finland');
INSERT INTO `countries` VALUES ('66', 'Fiji');
INSERT INTO `countries` VALUES ('67', 'Falkland Islands (Ma');
INSERT INTO `countries` VALUES ('68', 'Micronesia, Federate');
INSERT INTO `countries` VALUES ('69', 'Faroe Islands');
INSERT INTO `countries` VALUES ('70', 'France');
INSERT INTO `countries` VALUES ('71', 'France, Metropolitan');
INSERT INTO `countries` VALUES ('72', 'Gabon');
INSERT INTO `countries` VALUES ('73', 'Grenada');
INSERT INTO `countries` VALUES ('74', 'Georgia');
INSERT INTO `countries` VALUES ('75', 'French Guiana');
INSERT INTO `countries` VALUES ('76', 'Ghana');
INSERT INTO `countries` VALUES ('77', 'Gibraltar');
INSERT INTO `countries` VALUES ('78', 'Greenland');
INSERT INTO `countries` VALUES ('79', 'Gambia');
INSERT INTO `countries` VALUES ('80', 'Guinea');
INSERT INTO `countries` VALUES ('81', 'Guadeloupe');
INSERT INTO `countries` VALUES ('82', 'Equatorial Guinea');
INSERT INTO `countries` VALUES ('83', 'Greece');
INSERT INTO `countries` VALUES ('84', 'South Georgia and th');
INSERT INTO `countries` VALUES ('85', ' Islands');
INSERT INTO `countries` VALUES ('86', 'Guatemala');
INSERT INTO `countries` VALUES ('87', 'Guam');
INSERT INTO `countries` VALUES ('88', 'Guinea-bissau');
INSERT INTO `countries` VALUES ('89', 'Guyana');
INSERT INTO `countries` VALUES ('90', 'Hong Kong');
INSERT INTO `countries` VALUES ('91', 'Heard and Mc Donald ');
INSERT INTO `countries` VALUES ('92', 'Honduras');
INSERT INTO `countries` VALUES ('93', 'Croatia');
INSERT INTO `countries` VALUES ('94', 'Haiti');
INSERT INTO `countries` VALUES ('95', 'Hungary');
INSERT INTO `countries` VALUES ('96', 'Indonesia');
INSERT INTO `countries` VALUES ('97', 'Ireland');
INSERT INTO `countries` VALUES ('98', 'Israel');
INSERT INTO `countries` VALUES ('99', 'India');
INSERT INTO `countries` VALUES ('100', 'British Indian Ocean');
INSERT INTO `countries` VALUES ('101', 'Iraq');
INSERT INTO `countries` VALUES ('102', 'Iran (Islamic Republ');
INSERT INTO `countries` VALUES ('103', 'Iceland');
INSERT INTO `countries` VALUES ('104', 'Italy');
INSERT INTO `countries` VALUES ('105', 'Jamaica');
INSERT INTO `countries` VALUES ('106', 'Jordan');
INSERT INTO `countries` VALUES ('107', 'Japan');
INSERT INTO `countries` VALUES ('108', 'Kenya');
INSERT INTO `countries` VALUES ('109', 'Kyrgyzstan');
INSERT INTO `countries` VALUES ('110', 'Cambodia');
INSERT INTO `countries` VALUES ('111', 'Kiribati');
INSERT INTO `countries` VALUES ('112', 'Comoros');
INSERT INTO `countries` VALUES ('113', 'Saint Kitts and Nevi');
INSERT INTO `countries` VALUES ('114', 'Korea, Democratic Pe');
INSERT INTO `countries` VALUES ('115', ' Republic of');
INSERT INTO `countries` VALUES ('116', 'Korea, Republic of');
INSERT INTO `countries` VALUES ('117', 'Kuwait');
INSERT INTO `countries` VALUES ('118', 'Cayman Islands');
INSERT INTO `countries` VALUES ('119', 'Kazakhstan');
INSERT INTO `countries` VALUES ('120', 'Lao People\'s Democra');
INSERT INTO `countries` VALUES ('121', 'Lebanon');
INSERT INTO `countries` VALUES ('122', 'Saint Lucia');
INSERT INTO `countries` VALUES ('123', 'Liechtenstein');
INSERT INTO `countries` VALUES ('124', 'Sri Lanka');
INSERT INTO `countries` VALUES ('125', 'Liberia');
INSERT INTO `countries` VALUES ('126', 'Lesotho');
INSERT INTO `countries` VALUES ('127', 'Lithuania');
INSERT INTO `countries` VALUES ('128', 'Luxembourg');
INSERT INTO `countries` VALUES ('129', 'Latvia');
INSERT INTO `countries` VALUES ('130', 'Libyan Arab Jamahiri');
INSERT INTO `countries` VALUES ('131', 'Morocco');
INSERT INTO `countries` VALUES ('132', 'Monaco');
INSERT INTO `countries` VALUES ('133', 'Moldova, Republic of');
INSERT INTO `countries` VALUES ('134', 'Madagascar');
INSERT INTO `countries` VALUES ('135', 'Marshall Islands');
INSERT INTO `countries` VALUES ('136', 'Macedonia, The Forme');
INSERT INTO `countries` VALUES ('137', ' Republic of');
INSERT INTO `countries` VALUES ('138', 'Mali');
INSERT INTO `countries` VALUES ('139', 'Myanmar');
INSERT INTO `countries` VALUES ('140', 'Mongolia');
INSERT INTO `countries` VALUES ('141', 'Macau');
INSERT INTO `countries` VALUES ('142', 'Northern Mariana Isl');
INSERT INTO `countries` VALUES ('143', 'Martinique');
INSERT INTO `countries` VALUES ('144', 'Mauritania');
INSERT INTO `countries` VALUES ('145', 'Montserrat');
INSERT INTO `countries` VALUES ('146', 'Malta');
INSERT INTO `countries` VALUES ('147', 'Mauritius');
INSERT INTO `countries` VALUES ('148', 'Maldives');
INSERT INTO `countries` VALUES ('149', 'Malawi');
INSERT INTO `countries` VALUES ('150', 'Mexico');
INSERT INTO `countries` VALUES ('151', 'Malaysia');
INSERT INTO `countries` VALUES ('152', 'Mozambique');
INSERT INTO `countries` VALUES ('153', 'Namibia');
INSERT INTO `countries` VALUES ('154', 'New Caledonia');
INSERT INTO `countries` VALUES ('155', 'Niger');
INSERT INTO `countries` VALUES ('156', 'Norfolk Island');
INSERT INTO `countries` VALUES ('157', 'Nigeria');
INSERT INTO `countries` VALUES ('158', 'Nicaragua');
INSERT INTO `countries` VALUES ('159', 'Netherlands');
INSERT INTO `countries` VALUES ('160', 'Norway');
INSERT INTO `countries` VALUES ('161', 'Nepal');
INSERT INTO `countries` VALUES ('162', 'Nauru');
INSERT INTO `countries` VALUES ('163', 'Niue');
INSERT INTO `countries` VALUES ('164', 'New Zealand');
INSERT INTO `countries` VALUES ('165', 'Oman');
INSERT INTO `countries` VALUES ('166', 'Panama');
INSERT INTO `countries` VALUES ('167', 'Peru');
INSERT INTO `countries` VALUES ('168', 'French Polynesia');
INSERT INTO `countries` VALUES ('169', 'Papua New Guinea');
INSERT INTO `countries` VALUES ('170', 'Philippines');
INSERT INTO `countries` VALUES ('171', 'Pakistan');
INSERT INTO `countries` VALUES ('172', 'Poland');
INSERT INTO `countries` VALUES ('173', 'St. Pierre and Mique');
INSERT INTO `countries` VALUES ('174', 'Pitcairn');
INSERT INTO `countries` VALUES ('175', 'Puerto Rico');
INSERT INTO `countries` VALUES ('176', 'Portugal');
INSERT INTO `countries` VALUES ('177', 'Palau');
INSERT INTO `countries` VALUES ('178', 'Paraguay');
INSERT INTO `countries` VALUES ('179', 'Qatar');
INSERT INTO `countries` VALUES ('180', 'Reunion');
INSERT INTO `countries` VALUES ('181', 'Romania');
INSERT INTO `countries` VALUES ('182', 'Russian Federation');
INSERT INTO `countries` VALUES ('183', 'Rwanda');
INSERT INTO `countries` VALUES ('184', 'Saudi Arabia');
INSERT INTO `countries` VALUES ('185', 'Solomon Islands');
INSERT INTO `countries` VALUES ('186', 'Seychelles');
INSERT INTO `countries` VALUES ('187', 'Sudan');
INSERT INTO `countries` VALUES ('188', 'Sweden');
INSERT INTO `countries` VALUES ('189', 'Singapore');
INSERT INTO `countries` VALUES ('190', 'St. Helena');
INSERT INTO `countries` VALUES ('191', 'Slovenia');
INSERT INTO `countries` VALUES ('192', 'Svalbard and Jan May');
INSERT INTO `countries` VALUES ('193', 'Slovakia (Slovak Rep');
INSERT INTO `countries` VALUES ('194', 'Sierra Leone');
INSERT INTO `countries` VALUES ('195', 'San Marino');
INSERT INTO `countries` VALUES ('196', 'Senegal');
INSERT INTO `countries` VALUES ('197', 'Somalia');
INSERT INTO `countries` VALUES ('198', 'Suriname');
INSERT INTO `countries` VALUES ('199', 'Sao Tome and Princip');
INSERT INTO `countries` VALUES ('200', 'El Salvador');
INSERT INTO `countries` VALUES ('201', 'Syrian Arab Republic');
INSERT INTO `countries` VALUES ('202', 'Swaziland');
INSERT INTO `countries` VALUES ('203', 'Turks and Caicos Isl');
INSERT INTO `countries` VALUES ('204', 'Chad');
INSERT INTO `countries` VALUES ('205', 'French Southern Terr');
INSERT INTO `countries` VALUES ('206', 'Togo');
INSERT INTO `countries` VALUES ('207', 'Thailand');
INSERT INTO `countries` VALUES ('208', 'Tajikistan');
INSERT INTO `countries` VALUES ('209', 'Tokelau');
INSERT INTO `countries` VALUES ('210', 'Turkmenistan');
INSERT INTO `countries` VALUES ('211', 'Tunisia');
INSERT INTO `countries` VALUES ('212', 'Tonga');
INSERT INTO `countries` VALUES ('213', 'East Timor');
INSERT INTO `countries` VALUES ('214', 'Turkey');
INSERT INTO `countries` VALUES ('215', 'Trinidad and Tobago');
INSERT INTO `countries` VALUES ('216', 'Tuvalu');
INSERT INTO `countries` VALUES ('217', 'Taiwan');
INSERT INTO `countries` VALUES ('218', 'Tanzania, United Rep');
INSERT INTO `countries` VALUES ('219', 'Ukraine');
INSERT INTO `countries` VALUES ('220', 'Uganda');
INSERT INTO `countries` VALUES ('221', 'United Kingdom');
INSERT INTO `countries` VALUES ('222', 'United States Minor ');
INSERT INTO `countries` VALUES ('223', ' Islands');
INSERT INTO `countries` VALUES ('224', 'United States');
INSERT INTO `countries` VALUES ('225', 'Uruguay');
INSERT INTO `countries` VALUES ('226', 'Uzbekistan');
INSERT INTO `countries` VALUES ('227', 'Vatican City State (');
INSERT INTO `countries` VALUES ('228', 'Saint Vincent and th');
INSERT INTO `countries` VALUES ('229', 'Venezuela');
INSERT INTO `countries` VALUES ('230', 'Virgin Islands (Brit');
INSERT INTO `countries` VALUES ('231', 'Virgin Islands (U.S.');
INSERT INTO `countries` VALUES ('232', 'Viet Nam');
INSERT INTO `countries` VALUES ('233', 'Vanuatu');
INSERT INTO `countries` VALUES ('234', 'Wallis and Futuna Is');
INSERT INTO `countries` VALUES ('235', 'Samoa');
INSERT INTO `countries` VALUES ('236', 'Yemen');
INSERT INTO `countries` VALUES ('237', 'Mayotte');
INSERT INTO `countries` VALUES ('238', 'Yugoslavia');
INSERT INTO `countries` VALUES ('239', 'South Africa');
INSERT INTO `countries` VALUES ('240', 'Zambia');
INSERT INTO `countries` VALUES ('241', 'Zaire');
INSERT INTO `countries` VALUES ('242', 'Zimbabwe');

-- ----------------------------
-- Table structure for galleries
-- ----------------------------
DROP TABLE IF EXISTS `galleries`;
CREATE TABLE `galleries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Gallery_Product` (`product_id`),
  CONSTRAINT `Gallery_Product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of galleries
-- ----------------------------

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `image` text NOT NULL,
  `store_id` int(11) unsigned NOT NULL,
  `weight` int(11) unsigned DEFAULT '1',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Product_Store` (`store_id`),
  CONSTRAINT `Product_Store` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Pebble In a Bubble', 'Float in the sky, collect stars and become the Invincible Pebble.\r\n\r\nPebble in a Bubble is an Android game developed by PROGmaatic Developer Network.\r\nPebble start to float into the sky using bubble. But there are lots of obstacles which can pop his bubble!\r\nMove up and down to avoid collision. Use your enormous power to catch the sky star and become invincible! \r\nMove fast or slow by collecting other two powers.\r\nLet’s collect the star as many as possible and make yourself the best invincible Pebble in the sky.', '0.99', '1', 'https://lh4.ggpht.com/j9beFQw9N30qzvZqJTrMLWHkrIMBfbGurJvO1KvBzaWWwVLbGPMKCtpPENQQ_Aw3zQ=h900', '1', '0', '1');

-- ----------------------------
-- Table structure for ratings
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `rating` tinyint(1) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Rating_Product` (`product_id`),
  CONSTRAINT `Rating_Product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ratings
-- ----------------------------

-- ----------------------------
-- Table structure for stores
-- ----------------------------
DROP TABLE IF EXISTS `stores`;
CREATE TABLE `stores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Store_Admin` (`user_id`),
  CONSTRAINT `Store_Admin` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stores
-- ----------------------------
INSERT INTO `stores` VALUES ('1', 'PDN Store', 'Mobile Apps & Games', '1');
INSERT INTO `stores` VALUES ('2', 'Apple Store', 'Apple AppStore Banasree, Rampura', '1');
INSERT INTO `stores` VALUES ('3', 'Google Store', 'Google AppStore Banasree, Rampura', '2');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Dipu', 'dipu@progmaatic.com', '12345678', '1');
INSERT INTO `users` VALUES ('2', 'Nisat', 'nisat@progmaatic.com', '12345678', '1');
INSERT INTO `users` VALUES ('3', 'Rudro', 'rudro@progmaatic.com', '12345678', '1');
INSERT INTO `users` VALUES ('4', 'johndoe', 'john@doe.info', '8e653d41fdd006b6cf698ae4d3233fb3bd9a8b3d', '1');
INSERT INTO `users` VALUES ('5', 'phprocks', 'my@pass.co', '152810e69d3c13129acbfa4d3448f7dc1c99e50a', '1');
INSERT INTO `users` VALUES ('6', 'johndoe', 'john@doe.info', '8e653d41fdd006b6cf698ae4d3233fb3bd9a8b3d', '1');
INSERT INTO `users` VALUES ('7', 'phprocks', 'my@pass.co', '152810e69d3c13129acbfa4d3448f7dc1c99e50a', '1');
