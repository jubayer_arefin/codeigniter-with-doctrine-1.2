<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->view('welcome_message');
    }

    public function create_user() {
        $users = Doctrine_Core::getTable('Stores')->findAll(Doctrine_Core::HYDRATE_ARRAY);
        var_dump($users);
        exit;
        $Store = new Stores();
        $Store->name = 'Google Store';
        $Store->description = 'Google AppStore Banasree, Rampura';
        $user = Doctrine_Core::getTable('Users')->find(2);
        $Store->Users = $user;
        $Store->save();

        $User = new Users;
        $User->name = 'johndoe';
        $User->email = 'john@doe.info';
        $User->password = 'iamjohn';
        $User->status = 1;
        $User->save();

        echo "Added 1 User";
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */